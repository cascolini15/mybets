package forms;




import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import MyBets.servlet.MyBetsServlet;

import java.util.Map;

import WebManager.ActionResponse;


public abstract class FormHandler {

public ActionResponse handleResponse;
	
	public abstract ActionResponse handle(MyBetsServlet servlet, HttpServletRequest request, HttpServletResponse response, Map<String, String[]> parameters);
	
	
	public ActionResponse getActionResponse(){
		return this.handleResponse;
}
}