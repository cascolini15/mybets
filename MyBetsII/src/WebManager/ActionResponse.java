package WebManager;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import HTTPClient.Request;

public class ActionResponse {

	private String outputMessage;
	private List<String> errorMessages;
	private Object objectResult;
	private String redirectPage;
	
	public ActionResponse() {
		errorMessages = new LinkedList<String>();
		redirectPage = "";
	}
	
	public String getOutputMessage() {
		return outputMessage;
	}
	
	public void setOutputMessage(String outputMessage) {
		this.outputMessage = outputMessage;
	}
	
	public void addErrorMessage(String message){
		this.errorMessages.add(message);
	}
	
	public Collection<String> getErrorMessages(){
		return this.errorMessages;
	}
	
	public boolean hasErrorMessages(){
		return !this.errorMessages.isEmpty();
	}
	
	public Object getObjectResult() {
		return objectResult;
	}
	
	public void setObjectResult(Object objectResult) {
		this.objectResult = objectResult;
	}
	
	public void setRedirectPage(String page){
		this.redirectPage = page;
	}
	
	public String getRedirectPage(){
		return this.redirectPage;
	}
	
	@Override
	public String toString(){
		String ret = "OUTPUT MESSAGE: "+this.outputMessage;
		for(String err : this.errorMessages){
			ret += "\nERROR: "+err;
		}
		return ret;
	}
}
