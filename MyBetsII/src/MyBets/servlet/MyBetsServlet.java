package MyBets.servlet;



import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Constants.Application;
import Constants.Pages;
import WebManager.ActionResponse;

import forms.FormHandler;


@WebServlet("/MyBetsServlet")
public class MyBetsServlet extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	ServletConfig config;
	
	
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	 @Override
	public void init(ServletConfig config) throws ServletException{
	    	
	    	super.init(config);
	    	
	    	Application.SERVLET_PATH = this.getServletContext().getRealPath("");   	
       
	    }  	
	 public MyBetsServlet(){
			super();
		}
	 
	 @Override
		public void destroy() {
				// TODO Auto-generated method stub
			}
	
	 
	 
	 @Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html;charset=ISO-8859-1");
			
			System.out.println("NO GET");
			String referer = request.getHeader("Referer");
			request.setAttribute("referer", referer);
			
			String requestURI = request.getRequestURI();
			String queryString = request.getQueryString();
			System.out.println("\tREQUESTURI: "+requestURI);
			System.out.println("\tQUERY STRING: " + queryString);
			
			Map<String, String> queryStringParams = ServletUtils.parseQueryString(queryString);
			
			
			String[] uriParams = requestURI.split("/");
			
			String objectName = uriParams[2];
			String action = "";
			int id = -1;
			if(uriParams.length>3)
				action = uriParams[3];
			if(uriParams.length>4)
				id = Integer.valueOf(uriParams[4]);
				
			
				System.out.println("GET METHOD");
				
				
				HttpSession session = (HttpSession) request.getSession(true);
				
			
				
				
				
			
				
				ActionResponse actionResponse = (ActionResponse)session.getAttribute("actionResponse");
				session.removeAttribute("actionResponse");
				request.setAttribute("actionResponse", actionResponse);
	 
	 switch(objectName){
		case "index":
			
			forward(request, response, objectName);
			break;
		default:
			forward(request, response, "index");
			break;
		}
	 
	 
	 }
	 
	 
	 
	 
	 		
	 
	 public void forward(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException{
		
			getServletContext().getRequestDispatcher("/jsp/"+page+".jsp").forward(request, response);
		}
	 
	 @Override
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			request.setCharacterEncoding("ISO-8859-1");
			String formName = request.getParameter("formName");
			
			Class c;
			try {
				c = Class.forName("cte.mms.aplication.forms."+formName+"Form");
				Constructor ct = c.getConstructor(MyBetsServlet.class, HttpServletRequest.class, HttpServletResponse.class, Map.class);
				FormHandler fh = (FormHandler) ct.newInstance(this,request,response,request.getParameterMap());
				ActionResponse actionResponse = fh.getActionResponse();
				request.getSession().setAttribute("actionResponse", actionResponse);
				if(!actionResponse.getRedirectPage().equals("")){
					response.sendRedirect(actionResponse.getRedirectPage());
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | ClassNotFoundException | NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
	 
	 
	    @Override
		public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
			System.out.println("-------------------------------------------------------------");
			System.out.println("New Request | Method: "+request.getMethod());
			
			super.service(request, response);
		}
	
}
