package MyBets.servlet;



import java.util.HashMap;
import java.util.Map;

public class ServletUtils {

	public static Map<String, String> parseQueryString(String queryString){
		Map<String, String> queryStringParams = new HashMap<String, String>();
		try{
			String[] queryStringPairs = queryString.split("&");
			String queryStringKey = "";
			String queryStringValue = "";
			for(String queryStringPair : queryStringPairs){
				String[] aux = queryStringPair.split("=");
				try{
					queryStringKey = aux[0];
				}catch(NullPointerException e){
					queryStringKey = "";
				}
				
				try{
					queryStringValue = aux[1];
				}catch(NullPointerException e){
					queryStringValue = "";
				}
				
				queryStringParams.put(queryStringKey, queryStringValue);
			}
		}catch(Exception e){
			System.out.println("\tNO QUREY STRING MAP DEFINED");
		}
		return queryStringParams;
	}
	
}